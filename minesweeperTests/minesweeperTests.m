//
//  minesweeperTests.m
//  minesweeperTests
//
//  Created by Sonya Hochkammer on 12/12/14.
//  Copyright (c) 2014 Sonya Hochkammer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

#import "MinesweeperGame.h"

@interface minesweeperTests : XCTestCase

@end

@implementation minesweeperTests

MinesweeperGame *game;

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    game = [[MinesweeperGame alloc] initGameWithRows:4 columns:4 mines:2];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    
    //row for index
    XCTAssertEqual([game rowForIndex:2], 0);
    XCTAssertEqual([game rowForIndex:3], 0);
    XCTAssertEqual([game rowForIndex:11], 2);
    
    //column for index
    XCTAssertEqual([game colForIndex:1], 1);
    XCTAssertEqual([game colForIndex:15], 3);
    
    
    //index for row column
    XCTAssertEqual([game indexForRow:1 column:1], 5);
    XCTAssertEqual([game indexForRow:2 column:0], 8);
    XCTAssertEqual([game indexForRow:0 column:0], 0);
    XCTAssertEqual([game indexForRow:1 column:0], 4);
    
    //neighbors
    
    XCTAssertEqual([game westNeighborForIndex:1], 0);
    XCTAssertEqual([game eastNeighborForIndex:1], 2);
    XCTAssertEqual([game westNeighborForIndex:4], -1);
    XCTAssertEqual([game eastNeighborForIndex:3], -1);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
