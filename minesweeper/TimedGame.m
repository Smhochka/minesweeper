//
//  TimedGame.m
//  minesweeper
//
//  Created by Sonya Hochkammer on 4/28/15.
//  Copyright (c) 2015 Sonya Hochkammer. All rights reserved.
//

#import "TimedGame.h"

#define PENALTY_POINTS 75
#define BONUS_POINTS 100
#define FIRST_LEVEL 4
#define TIME_LIMIT 60.
#define TIME_REWARD 20.
#define MAX_MINES 14.

@implementation TimedGame

-(id)init{
    self = [super init];
    if(self){
        _score = 0;
        _multiplier =1;
        _level = 1;
        _mines = FIRST_LEVEL;
        _secondsRemaining = TIME_LIMIT;
    }
    return self;
}

-(void)incrementScore:(NSInteger)value{
    self.score += (value*self.multiplier);
}

-(void)increaseLevel{
    self.multiplier = self.multiplier+1;
    self.secondsRemaining += TIME_REWARD;
    self.level++;
    if(self.mines<11)
        self.mines+=2;
}

-(void)invokePenalty{
    self.score -= PENALTY_POINTS;
    if(self.score<0){
        self.score = 0;
    }
}

-(void)invokeBonus{
    self.score += self.multiplier*BONUS_POINTS;
}

-(bool)endGame{
    bool newHighScore = NO;
    if(self.score > [self highscore]){
        newHighScore = YES;
        [[NSUserDefaults standardUserDefaults] setInteger:self.score forKey:@"HighScore"];
    }
    return newHighScore;
}

-(NSInteger)highscore{
    return [[NSUserDefaults standardUserDefaults] integerForKey:@"HighScore"];
}

-(void)setHighscore:(NSInteger)highscore{
    self.highscore = highscore;
}

@end
