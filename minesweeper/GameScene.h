//
//  GameScene.h
//  minesweeper
//

//  Copyright (c) 2014 Sonya Hochkammer. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "MinesweeperGame.h"
#import "GameNode.h"

@interface GameScene : SKScene  <UIGestureRecognizerDelegate,GameDelegate>

@property (strong) MinesweeperGame* game;

-(void) resume;

@end
