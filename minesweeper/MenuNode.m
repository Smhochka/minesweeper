//
//  MenuNode.m
//  minesweeper
//
//  Created by Sonya Olson on 5/16/15.
//  Copyright (c) 2015 Sonya Hochkammer. All rights reserved.
//

#import "MenuNode.h"

#define MENU_COLOR [UIColor colorWithWhite:0. alpha:.75]

@interface MenuNode()

@property SKSpriteNode *rootLayer;
@property SKLabelNode *titleNode;
@property SKLabelNode *subtitleNode;
@property (strong) void (^completionBlock)(void);

@end

@implementation MenuNode

CFTimeInterval delay = 0;
CFTimeInterval menuOpened;

-(instancetype) init{
    self = [super init];
    if(self){
        _rootLayer = [SKSpriteNode spriteNodeWithColor:MENU_COLOR size:[[UIScreen mainScreen] bounds].size];
        [self addChild:self.rootLayer];
        [_rootLayer setHidden:YES];
        
        _titleNode = [SKLabelNode new];
        [_titleNode setFontSize:42];
        [_titleNode setColor:[UIColor whiteColor]];
        [_titleNode setPosition:CGPointMake(0,30)];
        [_rootLayer addChild:self.titleNode];
        
        _subtitleNode = [SKLabelNode labelNodeWithFontNamed:@"Helvetica-Neue"];
        [_subtitleNode setFontSize:17];
        [_subtitleNode setColor:[UIColor whiteColor]];
        [_subtitleNode setPosition:CGPointMake(0,-20)];
        [_rootLayer addChild:self.subtitleNode];
        
        _open = NO;
    }
    return self;
}

-(void) showMenuWithTitle:(NSString*) title subtitle:(NSString*)subtitle delay:(CFTimeInterval)wait completion:(void (^)(void))block{
    self.open = YES;
    [self.titleNode setText:title];
    [self.subtitleNode setText:subtitle];
    [self.rootLayer setHidden:NO];
    self.completionBlock = block;
    
    delay = wait;
    menuOpened = CFAbsoluteTimeGetCurrent();
    
    if(self.backgroundEffectNode){
        [self.backgroundEffectNode setShouldEnableEffects:YES];
    }
    
    [self setUserInteractionEnabled:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

-(void) closeMenu{
    self.open = NO;
    [self.rootLayer setHidden:YES];
    
    if(self.backgroundEffectNode){
        [self.backgroundEffectNode setShouldEnableEffects:NO];
    }
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    [self setUserInteractionEnabled:NO];
    
    if(self.completionBlock){
        self.completionBlock();
    }
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if(CFAbsoluteTimeGetCurrent() - menuOpened > delay){
        [self closeMenu];
    }else{
        delay = 0;
    }
}

@end
