//
//  GameRegion.m
//  minesweeper
//
//  Created by Sonya Hochkammer on 12/12/14.
//  Copyright (c) 2014 Sonya Hochkammer. All rights reserved.
//

#import "GameRegion.h"

@implementation GameRegion

@synthesize isMine;
@synthesize state;
@synthesize numberOfMineNeighbors;

-(id) init{
    self.state = GameRegionStateDefault;
    self.numberOfMineNeighbors = 0;
    
    return self;
}
-(GameRegionState)reveal{
    
    //unflag a flagged region
    if(state == GameRegionStateFlagged){
        self.state = GameRegionStateDefault;
        return GameRegionStateDefault;
    }
    
    self.state = GameRegionStateClear;
    return isMine ? GameRegionStateMine : GameRegionStateClear;
}

-(void)flag{
    self.state = GameRegionStateFlagged;
}
@end
