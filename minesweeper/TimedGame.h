//
//  TimedGame.h
//  minesweeper
//
//  Created by Sonya Hochkammer on 4/28/15.
//  Copyright (c) 2015 Sonya Hochkammer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MinesweeperGame.h"

@interface TimedGame : NSObject

@property (weak) MinesweeperGame *currentGame;
@property NSInteger score;
@property NSInteger multiplier;
@property NSInteger highscore;
@property NSInteger level;
@property NSInteger mines;
@property CFTimeInterval secondsRemaining;

-(void)incrementScore:(NSInteger) value;
-(void)increaseLevel;

-(void)invokePenalty;
-(void)invokeBonus;

-(bool)endGame;

@end
