//
//  GameNode.m
//  minesweeper
//
//  Created by Sonya Olson on 5/16/15.
//  Copyright (c) 2015 Sonya Hochkammer. All rights reserved.
//

#import "GameNode.h"

#define STATUS_HEIGHT 32.

#define REGION_SIZE_TINY 35.
#define REGION_SIZE 39.
#define REGION_SIZE_LARGE 44.

@interface GameNode()

@property (strong) SKSpriteNode *root;
@property (strong) SKSpriteNode *gameStatusLayer;
@property (strong) SKSpriteNode *flagModeLayer;

@end

@implementation GameNode

NSInteger regionSize = REGION_SIZE;

/*! Recursive function that clears the current region along
    with it's neighbors if it is not touching any mines.  It has the follwing
    terminal conditions:
 1. This region has already been cleared
 2. The game is over because the last mine has been marked (win)
 3. The game is over because the region cleared is a mine (loss)
 4. The region has been revealed and is touching one or more mines  
 
 @warning Game must be have status GameStatusInProgress
 */
-(void) clearRegionAtIndex:(NSInteger)index{
    
    //NSAssert([self.game status] == GameStatusInProgress, @"Attempted to clear index for completed game");
    if([self.game status] != GameStatusInProgress) return;
    
    GameRegionState regionState = [self.game gameRegionForIndex:index].state;
    SKSpriteNode *newSprite = [SKSpriteNode spriteNodeWithImageNamed:@"Region-Clear"];
    SKSpriteNode *sprite = [self.gameboardSprites objectAtIndex:index];
    
    // 1. Already cleared?
    if(regionState != GameRegionStateDefault){ return; }
    
    // 2. Game Won?
    if([self.game regionsRemaining] == 0){
        [self.game setStatus:GameStatusWon];
        [self endGame];
        return;
    }
    
    // 3. Game Lost?
    if([self.game revealRegionAtIndex:index] == GameRegionStateMine){
        newSprite = [SKSpriteNode spriteNodeWithImageNamed:@"Region-Mine"];
        newSprite.position = sprite.position;
        [self.minesLayer addChild:newSprite];
        
        [self.gameboardSprites replaceObjectAtIndex:index withObject:newSprite];
        [sprite removeFromParent];
        
        [self.game setStatus:GameStatusLost];
        [self endGame];
        return;
    }
    
    // Recur
    NSInteger number = ((GameRegion*)[self.game gameRegionForIndex:index]).numberOfMineNeighbors;
    NSInteger neighbor;
    NSInteger diagonalNeighbor;
    
    if(number==0){
        if((neighbor = [self.game northNeighborForIndex:index])>-1){
            [self clearRegionAtIndex:neighbor];
            
            if((diagonalNeighbor = [self.game eastNeighborForIndex:neighbor]) >1)
                [self clearRegionAtIndex:diagonalNeighbor];
            
            if((diagonalNeighbor = [self.game westNeighborForIndex:neighbor]) >1)
                [self clearRegionAtIndex:diagonalNeighbor];
        }
        
        if((neighbor = [self.game southNeighborForIndex:index])>-1){
            [self clearRegionAtIndex:neighbor];
            
            if((diagonalNeighbor = [self.game eastNeighborForIndex:neighbor]) >1)
                [self clearRegionAtIndex:diagonalNeighbor];
            
            diagonalNeighbor = [self.game westNeighborForIndex:neighbor];
            if(diagonalNeighbor >1)
                [self clearRegionAtIndex:diagonalNeighbor];
        }
        
        if((neighbor = [self.game eastNeighborForIndex:index])>-1){
            [self clearRegionAtIndex:neighbor];}
        
        if((neighbor = [self.game westNeighborForIndex:index])>-1){
            [self clearRegionAtIndex:neighbor];}
        
    }else{
        
        // 4. Touching a positive number of mines and should be labeled with the count
        newSprite = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:sprite.size];
        
        SKLabelNode* label = [SKLabelNode labelNodeWithFontNamed:@"Helvetica-Neue"];
        [label setText:@(number).stringValue];
        [label setFontSize:12.];
        [label setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
        [label setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
        [label setFontColor:[UIColor colorWithHue:(146.0/255.0) saturation:(43.0/255.0) brightness:(125.0/255.0) alpha:1]];
        [label setColor:[UIColor whiteColor]];
        [label setPosition:sprite.position];
        [self.minesLayer addChild:label];
        
        //also update the game score
        [self.gameDelegate incrementScore:number];
        
    }
    
    newSprite.position = sprite.position;
    [self.minesLayer addChild:newSprite];
    [self.gameboardSprites replaceObjectAtIndex:index withObject:newSprite];
    [sprite removeFromParent];
    
}

#pragma mark - Game Lifecyle

-(instancetype) init{
    self = [super init];
    if(self){
        self.root = [SKSpriteNode spriteNodeWithColor:[UIColor clearColor] size:[[UIScreen mainScreen] bounds].size];
        [self addChild:self.root];
        
        [self setup];
        
        switch((int)floor([[UIScreen mainScreen]bounds].size.height)){
            case 480: regionSize = REGION_SIZE_TINY; break;
            case 568: regionSize = REGION_SIZE; break;
            case 667:
            case 736: regionSize = REGION_SIZE_LARGE; break;
        }
    }
    return self;
}

-(void) update{
    [self reset];
    [self setup];
}

-(void)setup{
    
    self.flagModeLayer = [SKSpriteNode spriteNodeWithImageNamed:@"Flag"];
    [self.flagModeLayer setScale:0.75];
    [self.flagModeLayer setPosition:CGPointMake(0,0.35*[[UIScreen mainScreen] bounds].size.height)];
    [self.flagModeLayer setHidden:YES];
    [self.root addChild:self.flagModeLayer];
    
    self.gameStatusLayer = [SKSpriteNode spriteNodeWithImageNamed:@"Win"];
    self.gameStatusLayer.position = self.flagModeLayer.position;
    [self.gameStatusLayer setHidden:YES];
    [self.root addChild:self.gameStatusLayer];
    
    self.minesLayer = [SKNode node];
    self.minesLayer.position = CGPointMake(-regionSize*self.game.columns/2, -regionSize*self.game.rows/2);
    [self.root addChild:self.minesLayer];
    
    self.gameboardSprites = [NSMutableArray arrayWithCapacity:(self.game.rows*self.game.columns)];
    
    [self addRegionNodes];
}

-(void)endGame{
    NSString *imageName = self.game.status == GameStatusWon ? @"Win" : @"Lose";
    SKSpriteNode *newSprite = [SKSpriteNode spriteNodeWithImageNamed:imageName];
    newSprite.position = self.gameStatusLayer.position;
    
    [self.gameStatusLayer removeFromParent];
    self.gameStatusLayer = newSprite;
    [self.root addChild:self.gameStatusLayer];
    [self.gameStatusLayer setHidden:NO];
    
    [self.gameDelegate endGame];
}

-(void)reset{
    self.flagMode = NO;
    self.flagModeLayer.hidden = YES;
    
    self.gameStatusLayer.hidden = YES;
    
    self.gameboardSprites = nil;
    
    [self removeRegionNodes];
}

#pragma mark - Game Events

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    for (UITouch *touch in touches) {
        CGPoint location = [touch locationInNode:self.minesLayer];
        
        NSInteger column, row;
        if ([self convertPoint:location toColumn:&column row:&row]) {
            
            NSInteger index = [self.game indexForRow:row column:column];
            SKSpriteNode *sprite = [self.gameboardSprites objectAtIndex:index];
            GameRegionState regionState = [self.game gameRegionForIndex:index].state;
            
            /*  A game can be in progress, but not initialized (gameboard has not yet been determined).
                We wait on this to ensure that the first touch doesn't lose the game
             */
            if(!self.gameIsInitialized){
                [self.game randomizeMinesExcludingIndex:index];
                self.gameIsInitialized = YES;
            }
            
            if((regionState == GameRegionStateDefault && self.flagMode) || regionState == GameRegionStateFlagged){
                SKSpriteNode *newSprite;
                
                if(self.flagMode){
                    newSprite = [SKSpriteNode spriteNodeWithImageNamed:@"Region-Flag"];
                    [[self.game gameRegionForIndex:index] flag];
                }else{
                    newSprite = [SKSpriteNode spriteNodeWithImageNamed:@"Region"];
                    [[self.game gameRegionForIndex:index] reveal];
                    self.flagMode = NO;
                    [self.flagModeLayer setHidden:YES];
                }
                
                newSprite.position = sprite.position;
                [self.minesLayer addChild:newSprite];
                [self.gameboardSprites replaceObjectAtIndex:index withObject:newSprite];
                [sprite removeFromParent];
                
            }else{
                [self clearRegionAtIndex:index];
            }
        }
    }
    
    if([self.game regionsRemaining] == 0 && [self.game status] == GameStatusInProgress){
        [self.game setStatus:GameStatusWon];
        [self endGame];
    }
    
    self.flagMode = NO;
    [self.flagModeLayer setHidden:YES];
    
}


- (void) toggleFlagMode{
    if([self.game status] != GameStatusInProgress){
        return;
    }
    
    self.flagMode = !self.flagMode;
    [self.flagModeLayer setHidden:!self.flagMode];
}

#pragma mark - Helpers :)

-(void)addRegionNodes{
    SKAction *growIn =[SKAction scaleTo:1 duration:0.2];
    SKAction *fadeIn =[SKAction fadeAlphaTo:1 duration:0.2];
    
    [growIn setTimingMode:SKActionTimingEaseInEaseOut];
    [fadeIn setTimingMode:SKActionTimingEaseInEaseOut];
    
    for(int i = 0; i < self.game.rows*self.game.columns;i++){
        SKSpriteNode *sprite = [SKSpriteNode spriteNodeWithImageNamed:@"Region"];
        sprite.position = [self pointForColumn:[self.game colForIndex:i] row:[self.game rowForIndex:i]];
        [sprite setScale:.9];
        [sprite setAlpha:0];
        [sprite runAction:growIn];
        [sprite runAction:fadeIn];
        [self.gameboardSprites insertObject:sprite atIndex:i];
        [self.minesLayer addChild:sprite];
    }
}

-(void)removeRegionNodes{
    SKAction *shrinkOut =[SKAction scaleTo:.9 duration:0.2];
    SKAction *fadeOut =[SKAction fadeAlphaTo:0 duration:0.2];
    
    [shrinkOut setTimingMode:SKActionTimingEaseInEaseOut];
    [fadeOut setTimingMode:SKActionTimingEaseInEaseOut];
    
    self.scene.view.paused = NO;
    
    for(SKNode *child in self.minesLayer.children){
        [child runAction:fadeOut];
        [child runAction:shrinkOut completion:^(){
            [child removeFromParent];
        }];
    }
}

- (CGPoint)pointForColumn:(NSInteger)col row:(NSInteger)row {
    return CGPointMake(col*regionSize  + regionSize/2, row*regionSize + regionSize/2);
}

- (BOOL)convertPoint:(CGPoint)point toColumn:(NSInteger *)col row:(NSInteger *)row {
    NSParameterAssert(col);
    NSParameterAssert(row);
    
    // calculate the corresponding row and column numbers.
    if (point.x >= 0 && point.x < self.game.columns*regionSize &&
        point.y >= 0 && point.y < self.game.rows*regionSize) {
        
        *col = point.x / regionSize;
        *row = point.y / regionSize;
        return YES;
        
    } else {
        *col = NSNotFound;  // invalid location
        *row = NSNotFound;
        return NO;
    }
}

@end
