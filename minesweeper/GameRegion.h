//
//  GameRegion.h
//  minesweeper
//
//  Created by Sonya Hochkammer on 12/12/14.
//  Copyright (c) 2014 Sonya Hochkammer. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, GameRegionState) {
    GameRegionStateDefault = 0,
    GameRegionStateFlagged = 1,
    GameRegionStateMaybe = 2,
    GameRegionStateClear = 3,
    GameRegionStateMine =4
};

@interface GameRegion : NSObject

@property BOOL isMine;
@property GameRegionState state;
@property NSInteger numberOfMineNeighbors;

-(GameRegionState)reveal;
-(void)flag;

@end
