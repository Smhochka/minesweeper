//
//  Game.m
//  minesweeper
//
//  Created by Sonya Hochkammer on 12/12/14.
//  Copyright (c) 2014 Sonya Hochkammer. All rights reserved.
//

#import "MinesweeperGame.h"
#import "GameRegion.h"

@implementation MinesweeperGame

NSInteger mines;
NSMutableArray *gameboard;

-(id) initGameWithRows:(NSInteger)rows columns:(NSInteger)columns mines:(NSInteger)numMines{
    self = [super init];
    if(self){
    
        self.rows = rows;
        self.columns = columns;
        self.regionsRemaining = (self.rows * self.columns)-(long)numMines;
        mines = numMines;
        
        gameboard = [NSMutableArray arrayWithCapacity:(rows*columns)];
        
        for(int i = 0; i< (rows*columns); i++){
            [gameboard insertObject:[GameRegion new] atIndex:i];
        }
    
    }
    
    return self;
}

-(void) randomizeMinesExcludingIndex:(NSInteger)index{
    
    //setup the mines
    while(mines>0){
        NSInteger regionIndex = arc4random_uniform((u_int32_t) (self.rows*self.columns));
        GameRegion *region = [gameboard objectAtIndex:regionIndex];
        
        if(regionIndex != index && !region.isMine){
            [region setIsMine:YES];
            mines--;
        }
    }
    
    //update the mine counts
    for(int i = 0; i< (self.rows*self.columns); i++){
        GameRegion *region = [gameboard objectAtIndex:i];
        if(!region.isMine){
            [region setNumberOfMineNeighbors:[self numMineNeighborsForIndex:i]];
        }
    }
}

-(GameRegionState)revealRegionAtIndex:(NSInteger)index{
    GameRegion *region = [gameboard objectAtIndex:index];
    
    //break and return -1 if it's clear or a mine
    switch(region.state){
        case GameRegionStateClear:
            return -1;
            break;
        case GameRegionStateMine:
            return -1;
            break;
        case GameRegionStateFlagged:
        case GameRegionStateDefault:
        default:break;
    }
    
    self.regionsRemaining -=1;
    
    //reveal the item and return
    return [region reveal];
}


-(NSInteger) indexForRow:(NSInteger)row column:(NSInteger)column{
    return ((row) * (self.columns)) + column;
}

-(NSInteger) rowForIndex:(NSInteger)index{
    return (index / self.columns);
}

-(NSInteger) colForIndex:(NSInteger)index{
    return index % (self.rows);
}

-(GameRegion*) gameRegionForIndex:(NSInteger)index{
    return [gameboard objectAtIndex:index];
}

-(NSInteger) northNeighborForIndex:(NSInteger)index{
    NSInteger neighbor = index+self.columns;
    if(neighbor >= self.columns*self.rows)
        return -1;
    else
        return neighbor;
}
-(NSInteger) southNeighborForIndex:(NSInteger)index{
    NSInteger neighbor = index-self.columns;
    if(neighbor < 0)
        return -1;
    else
        return neighbor;
}
-(NSInteger) eastNeighborForIndex:(NSInteger)index{
    NSInteger neighbor = index+1;
    if(neighbor >= self.columns*self.rows || neighbor % (self.columns) == 0)
        return -1;
    else
        return index+1;
}
-(NSInteger) westNeighborForIndex:(NSInteger)index{
    NSInteger neighbor = index-1;
    if(index % (self.columns) == 0)
        return -1;
    else
        return neighbor;
}

-(NSInteger) numMineForIndex:(NSInteger)index{
    if(index>-1){
        return ((GameRegion*)[gameboard objectAtIndex:index]).isMine?1:0;
    }else{
        return 0;
    }
};

-(NSInteger) numMineNeighborsForIndex:(NSInteger)index{
    NSInteger number=0;
    
    //direct neighbors
    number += [self numMineForIndex:[self northNeighborForIndex:index]];
    number += [self numMineForIndex:[self southNeighborForIndex:index]];
    number += [self numMineForIndex:[self eastNeighborForIndex:index]];
    number += [self numMineForIndex:[self westNeighborForIndex:index]];
    
    NSInteger neighbor;
    
    //upper kitty corner
    neighbor = [self northNeighborForIndex:index];
    if(neighbor > 0){
        number+= [self numMineForIndex:[self eastNeighborForIndex:neighbor]];
        number+= [self numMineForIndex:[self westNeighborForIndex:neighbor]];
    }
    
    //lower kitty corner
    neighbor = [self southNeighborForIndex:index];
    if(neighbor >= 0){
        number+= [self numMineForIndex:[self eastNeighborForIndex:neighbor]];
        number+= [self numMineForIndex:[self westNeighborForIndex:neighbor]];
    }
    
    return number;
}

@end
