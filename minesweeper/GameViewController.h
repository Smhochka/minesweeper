//
//  GameViewController.h
//  minesweeper
//

//  Copyright (c) 2014 Sonya Hochkammer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface GameViewController : UIViewController

@end
