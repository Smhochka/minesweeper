//
//  GameScene.m
//  minesweeper
//
//  Created by Sonya Hochkammer on 12/12/14.
//  Copyright (c) 2014 Sonya Hochkammer. All rights reserved.
//

#import "GameScene.h"
#import "GameRegion.h"
#import "TimedGame.h"
#import "MenuNode.h"
#import "GameNode.h"

#define BAR_HEIGHT 35.
#define FONT @"Helvetica-Neue"
#define GRAY_COLOR [UIColor colorWithHue:(146.0/255.0) saturation:(43.0/255.0) brightness:(125.0/255.0) alpha:1]
#define LIGHT_GRAY_COLOR [UIColor colorWithHue:(140.0/255.0) saturation:(54.0/255.0) brightness:(201.0/255.0) alpha:.6]
#define RED_COLOR [UIColor colorWithHue:(249.0/255.0) saturation:(195.0/255.0) brightness:(244.0/255.0) alpha:1]
#define BLUR_RADIUS 15.

@interface GameScene()


@property (strong) TimedGame *timedGame;
@property (strong) MenuNode *menu;
@property (strong) GameNode *gameNode;

@property (strong) SKEffectNode *root;
@property (strong) SKShapeNode *barLayer;
@property (strong) SKShapeNode *circleLayer;
@property (strong) SKLabelNode *scoreLabel;
@property (strong) SKLabelNode *timeLabel;
@property (strong) SKLabelNode *levelLabel;
@property (strong) SKLabelNode *pauseButton;

@end

@implementation GameScene

NSInteger rows = 8;
NSInteger cols = 8;

CFTimeInterval lastUpdateTime;

Boolean lowTimeWarning = NO;
Boolean paused = NO;

#pragma mark - UI Events

-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    [self setupScene];
    [self setupTimedGame];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillResignActive)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:NULL];
    
    UISwipeGestureRecognizer *flagModeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(toggleFlagMode)];
    flagModeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionDown |UISwipeGestureRecognizerDirectionUp;
    flagModeGestureRecognizer.delegate = self;
    flagModeGestureRecognizer.numberOfTouchesRequired = 1;
    [self.scene.view addGestureRecognizer: flagModeGestureRecognizer];
    
}

- (void)update:(CFTimeInterval)currentTime {
    if(!paused){
        CFTimeInterval delta = currentTime - lastUpdateTime;
        lastUpdateTime = currentTime;
        if(delta>0){self.timedGame.secondsRemaining -= delta;}
        
        if(self.timedGame.secondsRemaining>0){
            [self.scoreLabel setText:@((int)[self.timedGame score]).stringValue];
            [self updateTimeLabel];
        }else{
            [self.timeLabel setText:@"Game Over"];
            [self endTimedGame];
        }
    }
}

-(void) applicationWillResignActive{
    if([self.gameNode gameIsInitialized] && (self.game.status == GameStatusInProgress) ){
        [self pause];
        [self.menu showMenuWithTitle:@"Paused"
                            subtitle:@"tap to resume"
                               delay:0
                          completion:^{
                              [self resume];
                          }];
    }
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    if([self.barLayer containsPoint:[[touches anyObject] locationInNode:self.gameNode]]){
        if(!paused && !self.menu.open){
            [self.menu showMenuWithTitle:@"Paused"
                                subtitle:@"tap to resume"
                                   delay:0
                              completion:^{
                                  [self resume];
                              }];
            [self pause];
        }
        return;
    }
    
    if([self.game status] != GameStatusInProgress){
        [self resetGame];
        return;
    }
    
    if(paused == YES){
        [self resume];
    }
    
    // pass touches along to game
    [self.gameNode touchesEnded:touches withEvent:event];
}

#pragma mark - Game Events

-(void) pause{
    paused = YES;
    [self.pauseButton setAlpha:.1];
}

-(void) resume{
    self.scene.view.paused = NO;
    lastUpdateTime = CFAbsoluteTimeGetCurrent();
    paused = NO;
    
    if([self.game status] != GameStatusInProgress){
        [self resetGame];
        return;
    }
    
    [self.pauseButton setAlpha:1];
    [self updateTimeLabel];
}

-(void) toggleFlagMode{
    [self.gameNode toggleFlagMode];
}

-(void)incrementScore:(NSInteger)value{
    [self.timedGame incrementScore:value];
}

#pragma mark - Game Lifecyle

-(void) endGame{    
    if(self.game.status == GameStatusWon){
        [self.timedGame increaseLevel];
        [self.timedGame invokeBonus];
    }else{
        [self.timedGame invokePenalty];
    }
    [self pause];
}

-(void) resetGame{
    [self cleanupGame];
    [self setupGame];
}

-(void) cleanupGame{
    [self.game setStatus:GameStatusInProgress];
    [self.gameNode setGameIsInitialized:NO];
    
    lowTimeWarning = NO;
    
    [self updateLevelLabel];
    
    [self.gameNode setGameIsInitialized:NO];
}

-(void) setupGame{

    self.game = [[MinesweeperGame alloc] initGameWithRows:rows columns:cols mines:self.timedGame.mines];
    
    //gameNode should update with new game
    self.gameNode.game = self.game;
    [self.gameNode update];
    
}

#pragma mark - Timed Game Lifecyle

/*!
 * @warning This should be called once on setup, from then it is automatically
 *          called by endTimedGame
 */
-(void) setupTimedGame{
    
    self.timedGame = [TimedGame new];
    [self pause];
    
    [self updateTimeLabel];
    [self updateLevelLabel];
    [self.scoreLabel setText:@((int)[self.timedGame score]).stringValue];
    
    [self setupGame];
}

-(void) endTimedGame{
    NSInteger score = self.timedGame.score;
    NSInteger highScore = [self.timedGame highscore];
    
    if([self.timedGame endGame]){
        [self.menu showMenuWithTitle:[NSString stringWithFormat:@"\u2605 %ld  ",(long)score]
                            subtitle:@"new highscore"
                               delay:5
                          completion:nil];
    }else{
        [self.menu showMenuWithTitle:[NSString stringWithFormat:@"score: %ld",(long)score]
                            subtitle:[NSString stringWithFormat:@"\u2605 best %ld",(long)highScore]
                               delay:5
                          completion:nil];
    }
    
    [self cleanupGame];
    [self setupTimedGame];
}

#pragma mark - Helpers :)

-(void)setupScene{
    
    CGFloat height = self.frame.size.height;
    CGFloat width = self.frame.size.width;
    
    //SKSpriteNode *background = [SKSpriteNode spriteNodeWithImageNamed:@"Background"];
    SKSpriteNode *background = [SKSpriteNode spriteNodeWithColor:[UIColor colorWithWhite:.95 alpha:1] size:self.size];
    [self addChild:background];
    
    self.root = [SKEffectNode node];
    [self.root setShouldEnableEffects:NO];
    CIFilter *blur = [CIFilter filterWithName:@"CIGaussianBlur" keysAndValues:@"inputRadius", @BLUR_RADIUS, nil];
    [self.root setFilter:blur];
    [self addChild:self.root];
    
    self.barLayer = [SKShapeNode shapeNodeWithRect:CGRectMake(-(width/2)-BLUR_RADIUS,height/2-BAR_HEIGHT,width+(2*BLUR_RADIUS),BAR_HEIGHT+BLUR_RADIUS)];
    [self.barLayer setFillColor:[UIColor colorWithWhite:1 alpha:1]];
    [self.barLayer setStrokeColor:[UIColor colorWithWhite:0 alpha:.1]];
    SKShapeNode *barLayerShadow = [self.barLayer copy];
    [barLayerShadow setGlowWidth:6.];
    [barLayerShadow setStrokeColor:[UIColor colorWithWhite:0 alpha:.03]];
    [self.root addChild:barLayerShadow];
    [self.root addChild:self.barLayer];
    
    self.menu = [MenuNode new];
    [self.menu setBackgroundEffectNode:self.root];
    [self addChild:self.menu];
    
    self.gameNode = [GameNode node];
    [self.gameNode setGameDelegate:self];
    [self.root addChild:self.gameNode];
    
    self.anchorPoint = CGPointMake(0.5, 0.5);
    
    self.timeLabel = [SKLabelNode labelNodeWithFontNamed:FONT];
    [self.timeLabel setPosition:CGPointMake((width/2)-10,(height/2)-18)];
    [self updateTimeLabel];
    [self.timeLabel setFontSize:17.];
    [self.timeLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [self.timeLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeRight];
    [self.barLayer addChild:self.timeLabel];
    
    self.pauseButton = [SKLabelNode labelNodeWithFontNamed:FONT];
    [self.pauseButton setPosition:CGPointMake(-(width/2)+10,(height/2)-18)];
    [self.pauseButton setText:@"PAUSE"];
    [self.pauseButton setFontSize:17.];
    [self.pauseButton setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [self.pauseButton setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeLeft];
    [self.barLayer addChild:self.pauseButton];
    
    UIBezierPath *circlePath = [UIBezierPath new];
    [circlePath addArcWithCenter:CGPointMake(0, -height/2) radius:width startAngle:M_PI endAngle:0 clockwise:NO];
    self.circleLayer = [SKShapeNode shapeNodeWithPath:circlePath.CGPath];
    [self.circleLayer setFillColor:[UIColor whiteColor]];
    [self.circleLayer setStrokeColor:[UIColor colorWithWhite:0 alpha:.1]];
    [self.circleLayer setPosition:CGPointMake(0,-width+75)];
    SKShapeNode *circleLayerShadow = [self.circleLayer copy];
    [circleLayerShadow setGlowWidth:6.];
    [circleLayerShadow setStrokeColor:[UIColor colorWithWhite:0 alpha:.03]];
    [self.root addChild:circleLayerShadow];
    [self.root addChild:self.circleLayer];
    
    self.scoreLabel = [SKLabelNode labelNodeWithFontNamed:FONT];
    [self.scoreLabel setPosition:CGPointMake(0,-((height/2)-50))];
    [self.scoreLabel setText:@"0"];
    [self.scoreLabel setScale:1];
    [self.scoreLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [self.scoreLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
    [self.scoreLabel setFontColor:GRAY_COLOR];
    [self.root addChild:self.scoreLabel];
    
    self.levelLabel = [SKLabelNode labelNodeWithFontNamed:FONT];
    [self.levelLabel setPosition:CGPointMake(0,-((self.frame.size.height/2)-20))];
    [self.levelLabel setFontSize:17.];
    [self updateLevelLabel];
    [self.levelLabel setVerticalAlignmentMode:SKLabelVerticalAlignmentModeCenter];
    [self.levelLabel setHorizontalAlignmentMode:SKLabelHorizontalAlignmentModeCenter];
    [self.levelLabel setFontColor:GRAY_COLOR];
    [self.root addChild:self.levelLabel];
    
}

- (void)updateLevelLabel{
    if(self.timedGame){
        [self.levelLabel setText:[NSString stringWithFormat:@"level %ld", (long) self.timedGame.level]];
    }else{
        [self.levelLabel setText:[NSString stringWithFormat:@"level 1"]];
    }
}

- (void)updateTimeLabel{
    
    NSInteger time = (NSInteger) self.timedGame.secondsRemaining;
    NSInteger seconds = time % 60;
    NSInteger minutes = (time / 60) % 60;
    
    [self.timeLabel setText:[NSString stringWithFormat:@"%02ld:%02ld", (long) minutes, (long) seconds]];
    
    if(time<10 &&!lowTimeWarning){
        lowTimeWarning = YES;
        SKAction *flashAction = [SKAction repeatActionForever:[SKAction sequence:@[[SKAction fadeInWithDuration:.1],
                                                                                   [SKAction waitForDuration:.8],
                                                                                   [SKAction fadeOutWithDuration:.1]]]];
        [self.timeLabel runAction:flashAction withKey:@"timeLabelAction"];
        [self.barLayer setFillColor:RED_COLOR];
        [self.timeLabel setFontColor:[UIColor whiteColor]];
        [self.pauseButton setFontColor:[UIColor whiteColor]];
        
    }else if((lowTimeWarning && time>=10)||time==0){
        if(time!=0){
            lowTimeWarning = NO;
        }
        [self.timeLabel removeActionForKey:@"timeLabelAction"];
        [self.barLayer setFillColor:[UIColor whiteColor]];
        [self.timeLabel setFontColor:GRAY_COLOR];
        [self.timeLabel setAlpha:1];
        [self.pauseButton setFontColor:RED_COLOR];
    }
}

@end
