//
//  GameNode.h
//  minesweeper
//
//  Created by Sonya Olson on 5/16/15.
//  Copyright (c) 2015 Sonya Hochkammer. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "MinesweeperGame.h"

@protocol GameDelegate

-(void) endGame;
-(void) incrementScore:(NSInteger)value;

@end

@interface GameNode : SKNode<UIGestureRecognizerDelegate>

@property (strong) NSMutableArray *gameboardSprites;
@property (strong) SKNode *minesLayer;
@property (weak,nonatomic) NSObject<GameDelegate>* gameDelegate;

@property (weak) MinesweeperGame *game;
@property BOOL flagMode;
@property BOOL gameIsInitialized;

-(void) reset;
-(void) update;
-(void) toggleFlagMode;

@end
