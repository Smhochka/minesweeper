//
//  MenuNode.h
//  minesweeper
//
//  Created by Sonya Olson on 5/16/15.
//  Copyright (c) 2015 Sonya Hochkammer. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "GameScene.h"

@interface MenuNode : SKNode

@property (weak) SKEffectNode *backgroundEffectNode;
@property bool open;

-(void) showMenuWithTitle:(NSString*) title
                 subtitle:(NSString*) subtitle
                    delay:(CFTimeInterval) wait
               completion:(void (^)(void)) block;
-(void) closeMenu;

@end
