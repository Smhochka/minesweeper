//
//  Game.h
//  minesweeper
//
//  Created by Sonya Hochkammer on 12/12/14.
//  Copyright (c) 2014 Sonya Hochkammer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GameRegion.h"

typedef NS_ENUM(NSInteger, GameStatus) {
    GameStatusInProgress = 0,
    GameStatusWon = 1,
    GameStatusLost = 2,
};

@interface MinesweeperGame : NSObject

@property NSInteger rows;
@property NSInteger columns;
@property GameStatus status;
@property NSInteger regionsRemaining;
@property NSInteger score;

-(id) initGameWithRows:(NSInteger)rows columns:(NSInteger)columns mines:(NSInteger)mines;
-(void) randomizeMinesExcludingIndex:(NSInteger)index;
-(GameRegionState)revealRegionAtIndex:(NSInteger)index;

-(NSInteger) indexForRow:(NSInteger)row column:(NSInteger)column;
-(NSInteger) colForIndex:(NSInteger)index;
-(NSInteger) rowForIndex:(NSInteger)index;
-(GameRegion*) gameRegionForIndex:(NSInteger)index;

-(NSInteger) northNeighborForIndex:(NSInteger)index;
-(NSInteger) southNeighborForIndex:(NSInteger)index;
-(NSInteger) eastNeighborForIndex:(NSInteger)index;
-(NSInteger) westNeighborForIndex:(NSInteger)index;
-(NSInteger) numMineForIndex:(NSInteger)index;

@end
