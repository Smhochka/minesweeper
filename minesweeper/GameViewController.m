//
//  GameViewController.m
//  minesweeper
//
//  Created by Sonya Hochkammer on 12/12/14.
//  Copyright (c) 2014 Sonya Hochkammer. All rights reserved.
//

#import "GameViewController.h"
#import "GameScene.h"
#import "MinesweeperGame.h"

SKScene *scene;

@implementation GameViewController

bool showTutorial = YES;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(showTutorial && ![[NSUserDefaults standardUserDefaults] objectForKey:@"SkipTutorial"]){
        UIView *tutorialView = [[[NSBundle mainBundle] loadNibNamed:@"TimedGameTutorialView" owner:self options:nil] objectAtIndex:0];
        [tutorialView setFrame:self.view.frame];
        [self.view addSubview:tutorialView];
    }else{
        [self presentScene];
        showTutorial = NO;
    }
}

-(void) presentScene{
    // Configure the view.
    SKView * skView = [SKView new];
    skView.frame = self.view.frame;
    skView.ignoresSiblingOrder = NO;
    //skView.showsFPS = YES;
    
    [self.view addSubview:skView];
    
    scene = [GameScene sceneWithSize:self.view.frame.size];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    
    //Present the scene.
    [skView presentScene:scene];
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    if(showTutorial){
        showTutorial =NO;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"SkipTutorial"];
        [self presentScene];
    }
    //[self.view setUserInteractionEnabled:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
